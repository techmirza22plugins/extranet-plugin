<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/public
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Extranet_Blue_Lynx_Plugin_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Extranet_Blue_Lynx_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Extranet_Blue_Lynx_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-fontawesome-css', 'https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-bootstap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-select2-css', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-jquery-datatable-css', plugin_dir_url( __FILE__ ) . 'css/jquery.dataTables.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/extranet-blue-lynx-plugin-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Extranet_Blue_Lynx_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Extranet_Blue_Lynx_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-bootstap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-select2-js', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery-loading-js', plugin_dir_url( __FILE__ ) . 'js/jquery.loading.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery-datatable-js', plugin_dir_url( __FILE__ ) . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/extranet-blue-lynx-plugin-public.js', array( 'jquery' ), $this->version, false );
		$extranetBlueLynxPluginSettings = get_option('extranet_blue_lynx_plugin_settings', array());
		$extranetBlueLynxVariableSettings = get_option('extranet_blue_lynx_variable_settings', array());
		$login_page_id = $extranetBlueLynxPluginSettings['login_page_id'];
		$register_page_id = $extranetBlueLynxPluginSettings['register_page_id'];
		$sorry_page_id = $extranetBlueLynxPluginSettings['sorry_page_id'];
		$confirm_email_page_id = $extranetBlueLynxPluginSettings['confirm_email_page_id'];
		$questionnaire_page_id = $extranetBlueLynxPluginSettings['questionnaire_page_id'];
		$upload_cv_page_id = $extranetBlueLynxPluginSettings['upload_cv_page_id'];
		$profile_page_id = $extranetBlueLynxPluginSettings['profile_page_id'];
		$dashboard_page_id = $extranetBlueLynxPluginSettings['dashboard_page_id'];
		$change_email_page_id = $extranetBlueLynxPluginSettings['change_email_page_id'];
		$change_password_page_id = $extranetBlueLynxPluginSettings['change_password_page_id'];
		$gdpr_page_id = $extranetBlueLynxPluginSettings['gdpr_page_id'];
		$my_jobs_page_id = $extranetBlueLynxPluginSettings['my_jobs_page_id'];
		$extranet_bluelynx_pages_array = array(
			'home_url' => site_url(),
			'domain_url' => $extranetBlueLynxVariableSettings['domain_url'],
			'login_page' => get_permalink((int) $login_page_id),
			'register_page' => get_permalink((int) $register_page_id),
			'sorry_page' => get_permalink((int) $sorry_page_id),
			'confirm_email_page' => get_permalink((int) $confirm_email_page_id),
			'questionnaire_page' => get_permalink((int) $questionnaire_page_id),
			'upload_cv_page' => get_permalink((int) $upload_cv_page_id),
			'profile_page' => get_permalink((int) $profile_page_id),
			'dashboard_page' => get_permalink((int) $dashboard_page_id),
			'change_email_page' => get_permalink((int) $change_email_page_id),
			'change_password_page' => get_permalink((int) $change_password_page_id),
			'gdpr_page' => get_permalink((int) $gdpr_page_id),
			'my_jobs_page' => get_permalink((int) $my_jobs_page_id),
		);
		wp_localize_script( $this->plugin_name, 'extranet_bluelynx_pages_array', $extranet_bluelynx_pages_array );

	}

	public function add_shortcodes() {
		add_shortcode( 'sh-login', array($this, 'sh_login_shortcode') );
		add_shortcode( 'sh-register', array($this, 'sh_register_shortcode') );
		add_shortcode( 'sh-forgot-password', array($this, 'sh_forgot_password_shortcode') );
		add_shortcode( 'sh-reset', array($this, 'sh_reset_password_shortcode') );
		add_shortcode( 'sh-questionnaire', array($this, 'sh_questionnaire_shortcode') );
		add_shortcode( 'sh-upload-cv', array($this, 'sh_upload_cv_shortcode') );
		add_shortcode( 'sh-profile', array($this, 'sh_profile_shortcode') );
		add_shortcode( 'sh-dashboard', array($this, 'sh_dashboard_shortcode') );
		add_shortcode( 'sh-change-email', array($this, 'sh_change_email_shortcode') );
		add_shortcode( 'sh-change-password', array($this, 'sh_change_password_shortcode') );
		add_shortcode( 'sh-gdpr', array($this, 'sh_gdpr_shortcode') );
		add_shortcode( 'sh-apply', array($this, 'sh_apply_shortcode') );
		add_shortcode( 'sh-my-jobs', array($this, 'sh_my_jobs_shortcode') );
		add_shortcode( 'sh-nav', array($this, 'sh_nav_shortcode') );
	}

	public function sh_login_shortcode() {
		ob_start();
        include_once 'partials/sh-login-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_register_shortcode() {
		ob_start();
        include_once 'partials/sh-register-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_forgot_password_shortcode() {
		ob_start();
        include_once 'partials/sh-forgot-password-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_reset_password_shortcode() {
		ob_start();
        include_once 'partials/sh-reset-password-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_questionnaire_shortcode() {
		ob_start();
        include_once 'partials/sh-questionnaire-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_upload_cv_shortcode() {
		ob_start();
        include_once 'partials/sh-upload-cv-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_profile_shortcode() {
		ob_start();
        include_once 'partials/sh-profile-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_dashboard_shortcode() {
		ob_start();
        include_once 'partials/sh-dashboard-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}
	
	public function sh_change_email_shortcode() {
		ob_start();
        include_once 'partials/sh-change-email-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_change_password_shortcode() {
		ob_start();
        include_once 'partials/sh-change-password-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_gdpr_shortcode() {
		ob_start();
        include_once 'partials/sh-gdpr-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_apply_shortcode() {
		ob_start();
        include_once 'partials/sh-apply-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_my_jobs_shortcode() {
		ob_start();
        include_once 'partials/sh-my-jobs-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function sh_nav_shortcode() {
		ob_start();
        include_once 'partials/sh-nav-shortcode.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function redirect_to_specific_page() {
		$token = $_GET['token'];
		$extranetBlueLynxPluginSettings = get_option('extranet_blue_lynx_plugin_settings', array());
		$extranetBlueLynxVariableSettings = get_option('extranet_blue_lynx_variable_settings', array());
		$confirm_email_page_id = $extranetBlueLynxPluginSettings['confirm_email_page_id'];
		if ( is_page((int) $confirm_email_page_id) && isset($token) && !empty($token) ) {
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => 'https://bluei2.bluelynx.com/api/confirm-email',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS =>'{
				"token": "'.$token.'"
				}',
				CURLOPT_HTTPHEADER => array(
					'Content-Type: application/json'
				),
			));
			$response = curl_exec($curl);
			curl_close($curl);
            $login_page_id = $extranetBlueLynxPluginSettings['login_page_id'];
			wp_redirect( get_permalink((int) $login_page_id) . '?confirm=true' );
		}
	}

}
