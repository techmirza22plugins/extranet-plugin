(function( $ ) {

	function validateEmail(email) {
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}

	function validatePassword(password) {
		const re = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{10,}$/;
		return re.test(password);
	}

	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;
	
		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');
	
			if (sParameterName[0] === sParam) {
				return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
		return false;
	};

	$.urlParam = function(url, name){
		var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
		if (results == null) {
		   return null;
		}
		return decodeURI(results[1]) || 0;
	}

	function hasExtension(inputID, exts) {
		var fileName = document.getElementById(inputID).value;
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
	}
	
	$(document).ready(function() {
		// SH Login Shortocde
		$(document).on("click", ".sh-login-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var emailAddress = currentForm.find("#email-address");
			var password = currentForm.find("#password");
			var privacyPolicy = currentForm.find("#privacy-policy");
			if ( !emailAddress.val() ) {
				currentForm.find(".alert-danger").html("Email address is required field.").show();
				return false;
			}
			if ( !validateEmail(emailAddress.val()) ) {
				currentForm.find(".alert-danger").html("Email address is not valid. Please try again.").show();
				return false;
			}
			if ( !password.val() ) {
				currentForm.find(".alert-danger").html("Password is required field.").show();
				return false;
			}
			if ( !privacyPolicy.prop("checked") ) {
				currentForm.find(".alert-danger").html("Privacy Policy is required field.").show();
				return false;
			}
			var inputData = {
				"email": emailAddress.val(),
				"password": password.val(),
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/login",
				"method": "POST",
				"timeout": 0,
				"headers": {
				  "Content-Type": "application/json"
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				localStorage.setItem("tokenBearer", response.access_token);
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/profile",
					"method": "GET",
					"timeout": 0,
					"headers": {
					  "Authorization": "Bearer " + response.access_token
					},
				};
				$.ajax(settings).done(function (response) {
					var record = response.record;
					var lastCV = record.last_cv;
					if ( lastCV.id ) {
						location.href = extranet_bluelynx_pages_array.dashboard_page;
					} else {
						location.href = extranet_bluelynx_pages_array.upload_cv_page
					}
				});
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Login Shortocde

		// SH Register Shortocde
		$(document).on("click", ".sh-register-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var lastName = currentForm.find("#last-name");
			var emailAddress = currentForm.find("#email-address");
			var password = currentForm.find("#password");
			var privacyPolicy = currentForm.find("#privacy-policy");
			if ( !emailAddress.val() ) {
				currentForm.find(".alert-danger").html("Email address is required field.").show();
				return false;
			}
			if ( !validateEmail(emailAddress.val()) ) {
				currentForm.find(".alert-danger").html("Email address is not valid. Please try again.").show();
				return false;
			}
			if ( !password.val() ) {
				currentForm.find(".alert-danger").html("Password is required field.").show();
				return false;
			}
			if ( !validatePassword(password.val()) ) {
				var html = "Passwod must be contain following: <br>";
				html += "Min 10 characters <br>";
				html += "At least 1 one lowercase letter <br>";
				html += "At least one uppercase letter <br>";
				html += "At least one digit <br>";
				html += "At least one special character";
				currentForm.find(".alert-danger").html(html).show();
				return false;
			}
			if ( !privacyPolicy.prop("checked") ) {
				currentForm.find(".alert-danger").html("Privacy Policy is required field.").show();
				return false;
			}
			var inputData = {
				"confirm_domain": extranet_bluelynx_pages_array.domain_url,
				"email": emailAddress.val(),
				"password": password.val(),
				"password_confirmation": password.val(),
				"last_name": lastName.val()
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/register",
				"method": "POST",
				"timeout": 0,
				"headers": {
				  "Content-Type": "application/json"
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				location.href = extranet_bluelynx_pages_array.login_page + '?register=true';
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.errors && xhr.responseJSON.errors.length > 0) {
					var html = "There are following errors from the server: <br>";
					xhr.responseJSON.errors.forEach(element => {
						html += element + " <br>";
					});
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Register Shortocde

		// SH Forgot Password Shortocde
		$(document).on("click", ".sh-forgot-password-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var emailAddress = currentForm.find("#email-address");
			if ( !emailAddress.val() ) {
				currentForm.find(".alert-danger").html("Email address is required field.").show();
				return false;
			}
			if ( !validateEmail(emailAddress.val()) ) {
				currentForm.find(".alert-danger").html("Email address is not valid. Please try again.").show();
				return false;
			}
			var inputData = {
				"email": emailAddress.val(),
				"confirm_domain": extranet_bluelynx_pages_array.domain_url,
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/send-forgotten-password-instructions",
				"method": "POST",
				"timeout": 0,
				"headers": {
				  "Content-Type": "application/json"
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				currentForm.find(".alert-success").html("Please confirm the reset email in your inbox.").show();
				return false;
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.errors && xhr.responseJSON.errors.length > 0) {
					var html = "There are following errors from the server: <br>";
					xhr.responseJSON.errors.forEach(element => {
						html += element + " <br>";
					});
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Forgot Password Shortocde

		// SH Reset Password Shortocde
		$(document).on("click", ".sh-reset-password-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var password = currentForm.find("#password");
			var confirmPassword = currentForm.find("#confirm-password");
			if ( !password.val() ) {
				currentForm.find(".alert-danger").html("Password is required field.").show();
				return false;
			}
			if ( !validatePassword(password.val()) ) {
				var html = "Passwod must be contain following: <br>";
				html += "Min 10 characters <br>";
				html += "At least 1 one lowercase letter <br>";
				html += "At least one uppercase letter <br>";
				html += "At least one digit <br>";
				html += "At least one special character";
				currentForm.find(".alert-danger").html(html).show();
				return false;
			}
			if ( !confirmPassword.val() ) {
				currentForm.find(".alert-danger").html("Confirm Password is required field.").show();
				return false;
			}
			if ( !validatePassword(confirmPassword.val()) ) {
				var html = "Confirm Passwod must be contain following: <br>";
				html += "Min 10 characters <br>";
				html += "At least 1 one lowercase letter <br>";
				html += "At least one uppercase letter <br>";
				html += "At least one digit <br>";
				html += "At least one special character";
				currentForm.find(".alert-danger").html(html).show();
				return false;
			}
			if ( password.val() != confirmPassword.val() ) {
				currentForm.find(".alert-danger").html("Password and Confirm Password should be same.").show();
				return false;
			}
			var inputData = {
				"token": getUrlParameter('token'),
				"password": password.val(),
				"password_confirmation": confirmPassword.val(),
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/reset-password",
				"method": "POST",
				"timeout": 0,
				"headers": {
				  "Content-Type": "application/json"
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				location.href = extranet_bluelynx_pages_array.login_page + '?reset-password=true';
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.errors && xhr.responseJSON.errors.length > 0) {
					var html = "There are following errors from the server: <br>";
					xhr.responseJSON.errors.forEach(element => {
						html += element + " <br>";
					});
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Reset Password Shortocde
		// SH Questionnaire Shortcode
		$(document).on("click", ".sh-questionnaire-shortcode .quiz label", function() {
			var currentQuiz = $(this).closest(".quiz");
			if ( currentQuiz.hasClass("quiz-1") ) {
				if ( $(this).find("input").val() == "1" ) {
					currentQuiz.addClass("hidden");
					currentQuiz.next().removeClass("hidden");
				} else {
					location.href = extranet_bluelynx_pages_array.sorry_page;
				}
			} else if ( currentQuiz.hasClass("quiz-2") ) {
				if ( $(this).find("input").val() == "1" ) {
					location.href = extranet_bluelynx_pages_array.register_page;
				} else {
					currentQuiz.addClass("hidden");
					currentQuiz.next().removeClass("hidden");
				}
			} else if ( currentQuiz.hasClass("quiz-3") ) {
				if ( $(this).find("input").val() == "1" ) {
					currentQuiz.addClass("hidden");
					currentQuiz.next().removeClass("hidden");
				} else {
					location.href = extranet_bluelynx_pages_array.sorry_page;
				}
			} else {
				if ( $(this).find("input").val() == "6" ) {
					location.href = extranet_bluelynx_pages_array.sorry_page;
				} else {
					location.href = extranet_bluelynx_pages_array.register_page;
				}
			}
		});
		// SH Questionnaire Shortcode

		// Drag Drop Code
		document.querySelectorAll(".drop-zone__input").forEach((inputElement) => {
			const dropZoneElement = inputElement.closest(".drop-zone");
			dropZoneElement.addEventListener("click", (e) => {
			  	inputElement.click();
			});
			inputElement.addEventListener("change", (e) => {
			  	if (inputElement.files.length) {
					updateThumbnail(dropZoneElement, inputElement.files[0]);
			  	}
			});
			dropZoneElement.addEventListener("dragover", (e) => {
			  	e.preventDefault();
			  	dropZoneElement.classList.add("drop-zone--over");
			});
			["dragleave", "dragend"].forEach((type) => {
			  	dropZoneElement.addEventListener(type, (e) => {
					dropZoneElement.classList.remove("drop-zone--over");
			  	});
			});
			dropZoneElement.addEventListener("drop", (e) => {
			  	e.preventDefault();
			  	if (e.dataTransfer.files.length) {
					inputElement.files = e.dataTransfer.files;
					updateThumbnail(dropZoneElement, e.dataTransfer.files[0]);
			  	}
			  	dropZoneElement.classList.remove("drop-zone--over");
			});
		});
		/**
		 * Updates the thumbnail on a drop zone element.
		 *
		 * @param {HTMLElement} dropZoneElement
		 * @param {File} file
		 */
		function updateThumbnail(dropZoneElement, file) {
			let thumbnailElement = dropZoneElement.querySelector(".drop-zone__thumb");
			// First time - remove the prompt
			if (dropZoneElement.querySelector(".drop-zone__prompt")) {
			  	dropZoneElement.querySelector(".drop-zone__prompt").remove();
			}
			// First time - there is no thumbnail element, so lets create it
			if (!thumbnailElement) {
			  	thumbnailElement = document.createElement("div");
			  	thumbnailElement.classList.add("drop-zone__thumb");
			  	dropZoneElement.appendChild(thumbnailElement);
			}
			thumbnailElement.dataset.label = file.name;
			// Show thumbnail for image files
			if (file.type.startsWith("image/")) {
			  	const reader = new FileReader();
				reader.readAsDataURL(file);
				reader.onload = () => {
					thumbnailElement.style.backgroundImage = `url('${reader.result}')`;
				};
			} else {
			  	thumbnailElement.style.backgroundImage = null;
			}
			$(document).find(".sh-upload-cv-shortcode .btn-remove-file").show();
		}
		// Drag Drop code

		// Remove Button code
		$(document).on("click", ".sh-upload-cv-shortcode .btn-remove-file", function() {
			var html = '<span class="drop-zone__prompt">Drop file here or click to upload</span><input type="file" name="myFile" class="drop-zone__input">';
			$(document).find(".drop-zone").html(html);
		});
		// Remove Button code

		// SH Upload CV Shortcode
		$(document).on("click", ".sh-upload-cv-shortcode .btn-submit", function() {
			var currentContainer = $(document).find(".sh-upload-cv-shortcode");
			if ( currentContainer.find(".drop-zone__input").get(0).files.length === 0 ) {
				currentContainer.find(".alert").hide();
				currentContainer.find(".alert.alert-danger").html("Please select your CV and then upload.").show();
			} else {
				if ( ! hasExtension("drop-zone-input", [".doc", ".docx", ".pdf"]) ) {
					currentContainer.find(".alert").hide();
					currentContainer.find(".alert.alert-danger").html("Only Doc, Docx and PDF files are allowed to upload. Please try again.").show();
					return false;
				}
				var form = new FormData();
				form.append("document", currentContainer.find(".drop-zone__input").get(0).files[0]);
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/profile/cv",
					"method": "POST",
					"timeout": 0,
					"headers": {
						"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					},
					"processData": false,
					"mimeType": "multipart/form-data",
					"contentType": false,
					"data": form
				};
				$.ajax(settings).done(function (response) {
					location.href = extranet_bluelynx_pages_array.profile_page;
				})
				.error(function (xhr, ajaxOptions, thrownError) {
					if (xhr.responseJSON.errors && xhr.responseJSON.errors.length > 0) {
						var html = "There are following errors from the server: <br>";
						xhr.responseJSON.errors.forEach(element => {
							html += element + " <br>";
						});
						currentForm.find(".alert-danger").html(html).show();
						return false;
					}
					if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
						var html = "There are following errors from the server: <br>";
						html += xhr.responseJSON.error;
						currentForm.find(".alert-danger").html(html).show();
						return false;
					}
				});
			}
		});
		// SH Upload CV Shortcode

		// Variables for SH Profile
		var languagesData = [];
		var jobTitlesData = [];
		var language_proficiency_options = [];
		var education_levels = [];
		var diploma_codes = [];

		// SH Profile on Load Data
		if ($(document).find(".sh-profile-shortcode").length > 0) {
			var personaliaTab = $(document).find(".sh-profile-shortcode #pills-personalia");
			var languageAndSkillsTab = $(document).find(".sh-profile-shortcode #pills-language-and-skills");
			var educationTab = $(document).find(".sh-profile-shortcode #pills-education");
			var jobExperienceTab = $(document).find(".sh-profile-shortcode #pills-job-experience");
			// Getting Nationalities
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/nationalities",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#nationalities").append(html);
				});
			});
			// Getting Countries
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/countries",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#country").append(html);
					personaliaTab.find("#preferred_countries").append(html);
				});
			});
			// Getting Country Regions / UK Regions
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/countries/35/regions",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#country_region_uk").append(html);
				});
			});
			// Getting NL Towns
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/nl-towns",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#nl_town").append(html);
				});
			});
			// Getting Languages
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/languages",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				languagesData = response.results;
			});
			// Getting Software Skills
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/software-skills",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.name+'">'+element.name+'</option>';
					languageAndSkillsTab.find("#software").append(html);
				});
			});
			// Getting Professional Skills
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/professional-skills",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				results.forEach(element => {
					var html = '<option value="'+element.name+'">'+element.name+'</option>';
					languageAndSkillsTab.find("#professional_skills").append(html);
				});
			});
			// Getting Job Titles
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/autocomplete/job-titles",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				jobTitlesData = response.results;
			});
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/profile",
				"method": "GET",
				"timeout": 0,
				"headers": {
				  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
				},
			};
			$.ajax(settings).done(function (response) {
				$(document).find(".sh-profile-shortcode").attr("data-id", response.record.id);
				var form_options = response.form_options;
				var gender_options = form_options.gender_options;
				var salary_currencies = form_options.salary_currencies;
				var salary_range_types = form_options.salary_range_types;
				var type_of_employment_options = form_options.type_of_employment_options;
				var full_parttime_options = form_options.full_parttime_options;
				var travelling_time_options = form_options.travelling_time_options;
				language_proficiency_options = form_options.language_proficiency_options;
				education_levels = form_options.education_levels;
				diploma_codes = form_options.diploma_codes;
				gender_options.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#gender").append(html);
				});
				salary_currencies.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#salary_currency").append(html);
				});
				salary_range_types.forEach(element => {
					var html = '<div class="custom-control custom-radio">';
					html += '<input type="radio" id="salary-range-type-'+element.name+'" name="salary_range_type" class="custom-control-input" value="'+element.name+'">';
					html += '<label class="custom-control-label" for="salary-range-type-'+element.name+'">'+element.name+'</label>';
					html += '</div>';
					personaliaTab.find(".salary-types-container").append(html);
				});
				type_of_employment_options.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#type_of_employment").append(html);
				});
				full_parttime_options.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#full_parttime").append(html);
				});
				travelling_time_options.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					personaliaTab.find("#travelling_time_id").append(html);
				});
				education_levels.forEach(element => {
					var html = '<option value="'+element.id+'">'+element.name+'</option>';
					educationTab.find("#highest_education_id").append(html);
				});
				// Filling the Details
				var record = response.record;
				personaliaTab.find(".first-name").val(record.first_name);
				personaliaTab.find(".middle-name").val(record.middle_name);
				personaliaTab.find(".last-name-preposition").val(record.last_name_preposition);
				personaliaTab.find(".last-name").val(record.last_name);
				personaliaTab.find(".initials").val(record.initials);
				personaliaTab.find(".gender").val(record.gender);
				personaliaTab.find(".date-of-birth").val(record.date_of_birth);
				if (record.nationalities) {
					record.nationalities.forEach(element => {
						personaliaTab.find(".nationalities").val(element.id);
					});
				}
				if (record.country.id) {
					personaliaTab.find(".country").val(record.country.id);
				}
				if (record.country_region.id) {
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-uk-col").show();
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-col").hide();
					personaliaTab.find(".country-region-uk").val(record.country_region.id);
				} else {
					personaliaTab.find(".country-region").val(record.province);
				}
				personaliaTab.find(".address").val(record.address);
				personaliaTab.find(".housenumber").val(record.housenumber);
				personaliaTab.find(".postcode").val(record.postcode);
				if (record.country.id == "28") {
					$(document).find(".sh-profile-shortcode #pills-personalia .nl-town-col").show();
					$(document).find(".sh-profile-shortcode #pills-personalia .town-col").hide();
					personaliaTab.find(".nl-town").val(record.town);
				} else {
					personaliaTab.find(".town").val(record.town);
				}
				personaliaTab.find(".address2").val(record.address2);
				personaliaTab.find(".phone-home").val(record.phone_home);
				personaliaTab.find(".phone-mobile").val(record.phone_mobile);
				personaliaTab.find(".linked-in").val(record.linked_in);
				personaliaTab.find(".email").val(record.email);
				personaliaTab.find(".personal-website").val(record.personal_website);
				personaliaTab.find(".drivers-license").val(record.drivers_license);
				if (record.preferred_countries) {
					record.preferred_countries.forEach(element => {
						personaliaTab.find(".preferred-countries").val(element.id);
					});
				}
				personaliaTab.find(".salary-range-min").val(record.salary_range_min);
				personaliaTab.find(".salary-range-max").val(record.salary_range_max);
				personaliaTab.find(".salary-currency").val(record.salary_currency);
				if (record.salary_range_type) {
					personaliaTab.find(".salary-types-container input").each(function() {
						if ($(this).attr("id") == "salary-range-type-" + record.salary_range_type) {
							$(this).prop("checked", true);
						} else {
							$(this).prop("checked", false);
						}
					});
				}
				if (record.salary_includes_vacation) {
					personaliaTab.find(".salary-includes-vacation").prop("checked", true);
				} else {
					personaliaTab.find(".salary-includes-vacation").prop("checked", false);
				}
				if (record.salary_is_negotiable) {
					personaliaTab.find(".salary-is-negotiable").prop("checked", true);
				} else {
					personaliaTab.find(".salary-is-negotiable").prop("checked", false);
				}
				if (record.freelancer_interest) {
					personaliaTab.find(".freelancer-interest").prop("checked", true);
				} else {
					personaliaTab.find(".freelancer-interest").prop("checked", false);
				}
				if (record.freelancer_day_rate) {
					personaliaTab.find(".freelancer-day-rate").val(record.freelancer_day_rate);
				}
				if (record.type_of_employment) {
					personaliaTab.find(".type-of-employment").val(record.type_of_employment);
				}
				if (record.full_parttime) {
					personaliaTab.find(".full-parttime").val(record.full_parttime);
				}
				if (record.travelling_time_id) {
					personaliaTab.find(".travelling-time-id").val(record.travelling_time_id);
				}
				personaliaTab.find(".select2").select2();
				if (record.languages.length > 0) {
					var languagesObj = record.languages;
					languagesObj.forEach(element => {
						languageAndSkillsTab.find(".add-new-lang-btn").click();
						var currentCard = languageAndSkillsTab.find(".language-card:first");
						currentCard.attr("data-id", element.id);
						if (element.language.id) {
							currentCard.find(".language-dropdown").val(element.language.id);
						}
						if (element.proficiency) {
							currentCard.find(".proficiency-option").each(function () {
								if ($(this).attr("value") == element.proficiency) {
									$(this).prop("checked", true);
								} else {
									$(this).prop("checked", false);
								}
							});
						}
					});
				}
				if (record.software.length > 0) {
					record.software.forEach(element => {
						var found = false;
						languageAndSkillsTab.find(".software-skills").each(function() {
							if ($(this).attr("value") == element) {
								$(this).attr("selected", "selected");
								found = true;
							}
						});
						if ( !found ) {
							var html = '<option value="'+element+'" selected="selected">'+element+'</option>';
							languageAndSkillsTab.find(".software-skills").append(html);
						}
					});
				}
				if (record.soft_skills.length > 0) {
					record.soft_skills.forEach(element => {
						var html = '<option value="'+element+'" selected="selected">'+element+'</option>';
						languageAndSkillsTab.find(".soft-skills").append(html);
					});
				}
				if (record.professional_skills.length > 0) {
					record.professional_skills.forEach(element => {
						var found = false;
						languageAndSkillsTab.find(".professional-skills").each(function() {
							if ($(this).attr("value") == element) {
								$(this).attr("selected", "selected");
								found = true;
							}
						});
						if ( !found ) {
							var html = '<option value="'+element+'" selected="selected">'+element+'</option>';
							languageAndSkillsTab.find(".professional-skills").append(html);
						}
					});
				}
				if (record.hobbies.length > 0) {
					record.hobbies.forEach(element => {
						var html = '<option value="'+element+'" selected="selected">'+element+'</option>';
						languageAndSkillsTab.find(".hobbies").append(html);
					});
				}
				if (record.ambitions) {
					languageAndSkillsTab.find(".ambitions").val(record.ambitions);
				}
				languageAndSkillsTab.find(".select2.tags").select2({
					tags: true
				});
				if (record.highest_education_id) {
					languageAndSkillsTab.find(".highest-education-id").val(record.highest_education_id);
				}
				if (record.education_history_items.length > 0) {
					var educationObj = record.education_history_items;
					educationObj.forEach(element => {
						educationTab.find(".add-new-education-btn").click();
						var currentCard = educationTab.find(".education-card:first");
						currentCard.attr("data-id", element.id);
						if (element.start_date) {
							currentCard.find(".start-date").val(element.start_date);
						}
						if (element.end_date) {
							currentCard.find(".end-date").val(element.end_date);
						}
						if (element.education_level.id) {
							currentCard.find(".education-level-id").val(element.education_level.id);
						}
						if (element.degree_direction) {
							currentCard.find(".degree-direction").val(element.degree_direction);
						}
						if (element.institute_name) {
							currentCard.find(".institute-name").val(element.institute_name);
						}
						if (element.institute_location) {
							currentCard.find(".institute-location").val(element.institute_location);
						}
						if (element.gpa) {
							currentCard.find(".gpa").val(element.gpa);
						}
						if (element.diploma_code.id) {
							currentCard.find(".diploma-code-id").val(element.diploma_code.id);
						}
					});
				}
				if (record.companies.length > 0) {
					record.companies.forEach(element => {
						var html = '<option value="'+element.id+'" selected="selected">'+element.name+'</option>';
						jobExperienceTab.find(".companies").append(html);
					});
				}
				if (record.industries.length > 0) {
					record.industries.forEach(element => {
						var html = '<option value="'+element.id+'" selected="selected">'+element.name+'</option>';
						jobExperienceTab.find(".industries").append(html);
					});
				}
				jobExperienceTab.find(".select2.tags").select2({
					tags: true
				});
				if (record.job_experiences.length > 0) {
					var jobExperiencesObj = record.job_experiences;
					jobExperiencesObj.forEach(element => {
						jobExperienceTab.find(".add-new-job-experience-btn").click();
						var currentCard = jobExperienceTab.find(".job-experience-card:first");
						currentCard.attr("data-id", element.id);
						if (element.start_date) {
							currentCard.find(".start-date").val(element.start_date);
						}
						if (element.end_date) {
							currentCard.find(".end-date").val(element.end_date);
						}
						if (element.job_title.id) {
							currentCard.find(".job-title").val(element.job_title.id);
						}
						if (element.employer_name) {
							currentCard.find(".employer-name").val(element.employer_name);
						}
						if (element.employer_place) {
							currentCard.find(".employer-place").val(element.employer_place);
						}
						if (element.description) {
							currentCard.find(".description").val(element.description);
						}
					});
				}
			});

			// On Click Add New Language
			$(document).on("click", ".add-new-lang-btn", function() {
				var rowIndex = $(document).find(".languages-container .language-card").length + 1;
				var html = '<div class="card language-card mt-3">';
				html += '<div class="card-body">';
				html += '<button type="button" class="btn btn-danger btn-circle btn-circle-close"><i class="fa fa-times"></i></button>';
				html += '<div class="row">';
				html += '<div class="col-sm-6">';
				html += '<div class="form-group">';
				html += '<label>Language</label>';
				html += '<select class="form-control language-dropdown" data-field="Language">';
				languagesData.forEach(element => {
					html += '<option value="'+element.id+'">'+element.name+'</option>';
				});
				html += '</select>';
				html += '</div>';
				html += '</div>';
				html += '<div class="col-sm-6">';
				html += '<div class="form-group">';
				html += '<label>Proficiency</label>';
				html += '<div class="row">';
				language_proficiency_options.forEach(element => {
					html += '<div class="col-sm-3">';
					html += '<div class="custom-control custom-radio">';
					html += '<input type="radio" id="proficiency-'+rowIndex+'-'+element.id+'" name="proficiency-'+rowIndex+'" value="'+element.id+'" class="custom-control-input proficiency-option">';
					html += '<label class="custom-control-label" for="proficiency-'+rowIndex+'-'+element.id+'">'+element.name+'</label>';
					html += '</div>';
					html += '</div>';
				});
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				html += '</div>';
				$(document).find(".languages-container").prepend(html);
			});

			// Close Button
			$(document).on("click", ".language-card .btn-circle-close", function() {
				$(this).closest(".language-card").remove();
			});

			// On Click Add New Education
			$(document).on("click", ".add-new-education-btn", function() {
				var rowIndex = $(document).find(".educations-container .education-card").length + 1;
				var html = '<div class="card education-card mt-3">';
                html += '<div class="card-body">';
                html += '<button type="button" class="btn btn-danger btn-circle btn-circle-close"><i class="fa fa-times"></i></button>';
                html += '<div class="row">';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="education_start_date_'+rowIndex+'">Start Date</label>';
                html += '<input type="date" class="form-control start-date" id="education_start_date_'+rowIndex+'" data-field="Start Date">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="education_end_date_'+rowIndex+'">End Date</label>';
                html += '<input type="date" class="form-control end-date" id="education_end_date_'+rowIndex+'" data-field="End Date">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="education_level_id_'+rowIndex+'">Education Level</label>';
                html += '<select name="education_level_id" id="education_level_id_'+rowIndex+'" class="form-control education-level-id" data-field="Education Level">';
				education_levels.forEach(element => {
					html += '<option value="'+element.id+'">'+element.name+'</option>';
				});
				html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="degree_direction_'+rowIndex+'">Degree Direction</label>';
                html += '<input type="text" class="form-control degree-direction" id="degree_direction_'+rowIndex+'" data-field="Degree Direction">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="institute_name_'+rowIndex+'">Institute Name</label>';
                html += '<input type="text" class="form-control institute-name" id="institute_name_'+rowIndex+'" data-field="Institute Name">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="institute_location_'+rowIndex+'">Institute Location</label>';
                html += '<input type="text" class="form-control institute-location" id="institute_location_'+rowIndex+'" data-field="Institute Location">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="gpa_'+rowIndex+'">GPA</label>';
                html += '<input type="text" class="form-control gpa" id="gpa_'+rowIndex+'" data-field="GPA">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="diploma_code_id_'+rowIndex+'">Diploma</label>';
                html += '<select name="diploma_code_id" id="diploma_code_id_'+rowIndex+'" class="form-control diploma-code-id" data-field="Diploma">';
				diploma_codes.forEach(element => {
					html += '<option value="'+element.id+'">'+element.name+'</option>';
				});
				html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
				$(document).find(".educations-container").prepend(html);
			});

			// Close Button
			$(document).on("click", ".education-card .btn-circle-close", function() {
				$(this).closest(".education-card").remove();
			});

			// On Click Add New Job Experience
			$(document).on("click", ".add-new-job-experience-btn", function() {
				var rowIndex = $(document).find(".job-experiences-container .job-experience-card").length + 1;
				var html = '<div class="card job-experience-card mt-3">';
                html += '<div class="card-body">';
                html += '<button type="button" class="btn btn-danger btn-circle btn-circle-close"><i class="fa fa-times"></i></button>';
                html += '<div class="row">';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="job_experience_start_date_'+rowIndex+'">Start Date</label>';
                html += '<input type="date" class="form-control start-date" id="job_experience_start_date_'+rowIndex+'" data-field="Start Date">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="job_experience_end_date_'+rowIndex+'">End Date</label>';
                html += '<input type="date" class="form-control end-date" id="job_experience_end_date_'+rowIndex+'" data-field="End Date">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="job_title_'+rowIndex+'">Job Title</label>';
                html += '<select name="job_title" id="job_title_'+rowIndex+'" class="form-control job-title" data-field="Job Title">';
				jobTitlesData.forEach(element => {
					html += '<option value="'+element.id+'">'+element.name+'</option>';
				});
				html += '</select>';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="employer_name_'+rowIndex+'">Employer Name</label>';
                html += '<input type="text" class="form-control employer-name" id="employer_name_'+rowIndex+'" data-field="Employer Name">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="employer_place_'+rowIndex+'">Employer Place</label>';
                html += '<input type="text" class="form-control employer-place" id="employer_place_'+rowIndex+'" data-field="Employer Place">';
                html += '</div>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<div class="form-group">';
                html += '<label for="description_'+rowIndex+'">Description</label>';
                html += '<input type="text" class="form-control description" id="description_'+rowIndex+'" data-field="Description">';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
				$(document).find(".job-experiences-container").prepend(html);
			});

			// On change Country
			$(document).on("change", ".sh-profile-shortcode #pills-personalia .country", function() {
				if ($(this).val() == "35") {
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-uk-col").show();
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-col").hide();
				} else {
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-uk-col").hide();
					$(document).find(".sh-profile-shortcode #pills-personalia .country-region-col").show();
				}
				if ($(this).val() == "28") {
					$(document).find(".sh-profile-shortcode #pills-personalia .nl-town-col").show();
					$(document).find(".sh-profile-shortcode #pills-personalia .town-col").hide();
				} else {
					$(document).find(".sh-profile-shortcode #pills-personalia .nl-town-col").hide();
					$(document).find(".sh-profile-shortcode #pills-personalia .town-col").show();
				}
			});

			// Profile Data Submit
			$(document).on("click", ".sh-profile-shortcode .update-profile-btn", function() {
				var profile_id = $(document).find(".sh-profile-shortcode").attr("data-id");
				var personaliaTab = $(document).find(".sh-profile-shortcode #pills-personalia");
				var languageAndSkillsTab = $(document).find(".sh-profile-shortcode #pills-language-and-skills");
				var educationTab = $(document).find(".sh-profile-shortcode #pills-education");
				var jobExperienceTab = $(document).find(".sh-profile-shortcode #pills-job-experience");
				var first_name = personaliaTab.find(".first-name").val();
				var middle_name = personaliaTab.find(".middle-name").val();
				var last_name_preposition = personaliaTab.find(".last-name-preposition").val();
				var last_name = personaliaTab.find(".last-name").val();
				var initials = personaliaTab.find(".initials").val();
				var gender = personaliaTab.find(".gender").val();
				var date_of_birth = personaliaTab.find(".date-of-birth").val();
				var languages = [];
				var languagesContainer = languageAndSkillsTab.find(".languages-container");
				languagesContainer.find(".language-card").each(function() {
					var obj = {};
					var id = null;
					var language_id = null;
					var proficiency = null;
					if ($(this).attr("data-id")) {
						id = parseInt($(this).attr("data-id"));
					}
					language_id = parseInt($(this).find(".language-dropdown").val());
					$(this).find(".proficiency-option").each(function() {
						if ($(this).prop("checked")) {
							proficiency = parseInt($(this).attr("value"));
						}
					});
					obj = {
						id: id,
						language_id: language_id,
						proficiency: proficiency
					};
					languages.push(obj);
				});
				var address = personaliaTab.find(".address").val();
				var housenumber = personaliaTab.find(".housenumber").val();
				var postcode = personaliaTab.find(".postcode").val();
				var town = "";
				var country_id = parseInt(personaliaTab.find(".country").val());
				var country_region_id = null;
				var province = "";
				if (country_id == 35) {
					country_region_id = parseInt(personaliaTab.find(".country-region-uk").val());
				} else {
					province = personaliaTab.find(".country-region").val();
				}
				if (country_id == 28) {
					town = personaliaTab.find(".nl-town").val();
				} else {
					town = personaliaTab.find(".town").val();
				}
				var address2 = personaliaTab.find(".address2").val();
				var phone_home = personaliaTab.find(".phone-home").val();
				var phone_mobile = personaliaTab.find(".phone-mobile").val();
				var linked_in = personaliaTab.find(".linked-in").val();
				var salary_is_negotiable = personaliaTab.find(".salary-is-negotiable").prop("checked");
				var salary_range_min = personaliaTab.find(".salary-range-min").val();
				var salary_range_max = personaliaTab.find(".salary-range-max").val();
				var salary_range_type = "";
				var salaryRageTypesContainer = personaliaTab.find(".salary-types-container");
				salaryRageTypesContainer.find(".custom-control-input").each(function() {
					if ($(this).prop("checked")) {
						salary_range_type = $(this).next().text();
					}
				});
				var salary_currency = personaliaTab.find(".salary-currency").val();
				var salary_includes_vacation = personaliaTab.find(".salary-includes-vacation").prop("checked");
				var freelancer_interest = personaliaTab.find(".freelancer-interest").prop("checked");
				var freelancer_day_rate = null;
				if (personaliaTab.find(".freelancer-day-rate").val()) {
					freelancer_day_rate = parseInt(personaliaTab.find(".freelancer-day-rate").val());
				}
				var identification = false;
				var identification_work_status_approved = false;
				var type_of_employment = personaliaTab.find(".type-of-employment").val();
				var full_parttime = personaliaTab.find(".full-parttime").val();
				var travelling_time_id = null;
				if (personaliaTab.find(".travelling-time-id").val()) {
					travelling_time_id = personaliaTab.find(".travelling-time-id").val();
				}
				var ambitions = languageAndSkillsTab.find(".ambitions").val();
				var hobbies = [];
				var hobbiesContainer = languageAndSkillsTab.find(".hobbies");
				hobbiesContainer.find("option").each(function() {
					if ($(this).attr("selected") && $(this).attr("selected") == "selected") {
						hobbies.push($(this).attr("value"));
					}
				});
				var soft_skills = [];
				var softSkillsContainer = languageAndSkillsTab.find(".soft-skills");
				softSkillsContainer.find("option").each(function() {
					if ($(this).attr("selected") && $(this).attr("selected") == "selected") {
						soft_skills.push($(this).attr("value"));
					}
				});
				var software = [];
				var softwareContainer = languageAndSkillsTab.find(".software-skills");
				softwareContainer.find("option").each(function() {
					if ($(this).attr("selected") && $(this).attr("selected") == "selected") {
						software.push($(this).attr("value"));
					}
				});
				var personal_website = personaliaTab.find(".personal-website").val();
				var drivers_license = personaliaTab.find(".drivers-license").val();
				var professional_skills = [];
				var professionalSkillsContainer = languageAndSkillsTab.find(".professional-skills");
				professionalSkillsContainer.find("option").each(function() {
					if ($(this).attr("selected") && $(this).attr("selected") == "selected") {
						professional_skills.push($(this).attr("value"));
					}
				});
				var nationalities = [];
				var nationalitiesContainer = personaliaTab.find(".nationalities");
				if (nationalitiesContainer.find("option").length > 0) {
					nationalitiesContainer.find("option").each(function() {
						if (nationalitiesContainer.val() && nationalitiesContainer.val().includes($(this).attr("value"))) {
							var obj = {
								id: parseInt($(this).attr("value")),
								name: $(this).text(),
								new_record: false
							};
							nationalities.push(obj);
						}
					});
				}
				var highest_education_id = educationTab.find(".highest-education-id").val();
				var education_items = [];
				var educationsContainer = educationTab.find(".educations-container");
				educationsContainer.find(".education-card").each(function() {
					var obj = {};
					var id = null;
					var start_date = "";
					var end_date = "";
					var degree_direction = "";
					var institute_name = "";
					var institute_location = "";
					var gpa = "";
					var education_level_id = null;
					var diploma_code_id = null;
					var _destroy = false;
					if ($(this).attr("data-id")) {
						id = parseInt($(this).attr("data-id"));
					}
					language_id = parseInt($(this).find(".language-dropdown").val());
					if ($(this).find(".start-date").val()) {
						start_date = $(this).find(".start-date").val();
					}
					if ($(this).find(".end-date").val()) {
						end_date = $(this).find(".end-date").val();
					}
					if ($(this).find(".degree-direction").val()) {
						degree_direction = $(this).find(".degree-direction").val();
					}
					if ($(this).find(".institute-name").val()) {
						institute_name = $(this).find(".institute-name").val();
					}
					if ($(this).find(".institute-location").val()) {
						institute_location = $(this).find(".institute-location").val();
					}
					if ($(this).find(".gpa").val()) {
						gpa = $(this).find(".gpa").val();
					}
					if ($(this).find(".education-level-id").val()) {
						education_level_id = parseInt($(this).find(".education-level-id").val());
					}
					if ($(this).find(".diploma-code-id").val()) {
						diploma_code_id = parseInt($(this).find(".diploma-code-id").val());
					}
					obj = {
						id: id,
						start_date: start_date,
						end_date: end_date,
						degree_direction: degree_direction,
						institute_name: institute_name,
						institute_location: institute_location,
						gpa: gpa,
						education_level_id: education_level_id,
						diploma_code_id: diploma_code_id,
						_destroy: _destroy,
					};
					education_items.push(obj);
				});
				var job_experiences = [];
				var jobExperiencesContainer = jobExperienceTab.find(".job-experiences-container");
				jobExperiencesContainer.find(".job-experience-card").each(function() {
					var obj = {};
					var id = null;
					var start_date = "";
					var end_date = "";
					var currently_employed = false;
					var employer_name = "";
					var employer_place = "";
					var description = "";
					var job_title_id = null;
					var _destroy = false;
					if ($(this).attr("data-id")) {
						id = parseInt($(this).attr("data-id"));
					}
					if ($(this).find(".start-date").val()) {
						start_date = $(this).find(".start-date").val();
					}
					if ($(this).find(".end-date").val()) {
						end_date = $(this).find(".end-date").val();
					}
					if ($(this).find(".employer-name").val()) {
						employer_name = $(this).find(".employer-name").val();
					}
					if ($(this).find(".employer-place").val()) {
						employer_place = $(this).find(".employer-place").val();
					}
					if ($(this).find(".description").val()) {
						description = $(this).find(".description").val();
					}
					if ($(this).find(".job-title").val()) {
						job_title_id = parseInt($(this).find(".job-title").val());
					}
					obj = {
						id: id,
						start_date: start_date,
						end_date: end_date,
						currently_employed: currently_employed,
						employer_name: employer_name,
						employer_place: employer_place,
						description: description,
						job_title_id: job_title_id,
						_destroy: _destroy,
					};
					job_experiences.push(obj);
				});
				var data = {
					"id" : profile_id,
					"first_name" : first_name,
					"middle_name" : middle_name,
					"last_name_preposition" : last_name_preposition,
					"last_name" : last_name,
					"initials" : initials,
					"gender" : gender,
					"date_of_birth" : date_of_birth,
					"languages" : languages,
					"address" : address,
					"housenumber" : housenumber,
					"postcode" : postcode,
					"town" : town,
					"country_id" : country_id,
					"country_region_id" : country_region_id,
					"province" : province,
					"address2" : address2,
					"phone_home" : phone_home,
					"phone_mobile" : phone_mobile,
					"linked_in" : linked_in,
					"salary_is_negotiable" : salary_is_negotiable,
					"salary_range_min" : salary_range_min,
					"salary_range_max" : salary_range_max,
					"salary_range_type" : salary_range_type,
					"salary_currency" : salary_currency,
					"salary_includes_vacation" : salary_includes_vacation,
					"freelancer_interest" : freelancer_interest,
					"freelancer_day_rate" : freelancer_day_rate,
					"identification" : identification,
					"identification_work_status_approved" : identification_work_status_approved,
					"type_of_employment" : type_of_employment,
					"full_parttime" : full_parttime,
					"travelling_time_id" : travelling_time_id,
					"ambitions" : ambitions,
					"hobbies" : hobbies,
					"soft_skills" : soft_skills,
					"software" : software,
					"errors" : {},
					"personal_website" : personal_website,
					"drivers_license" : drivers_license,
					"professional_skills" : professional_skills,
					"nationalities" : nationalities,
					"highest_education_id" : highest_education_id,
					"education_items" : education_items,
					"job_experiences" : job_experiences
				}
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/profile",
					"method": "POST",
					"timeout": 0,
					"headers": {
					  "Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					  "Content-Type": "application/json",
					},
					"data": JSON.stringify(data),
				};
				$.ajax(settings).done(function (response) {
					location.href = extranet_bluelynx_pages_array.profile_page + "?updated=true";
				});
			});
		}
		// SH Profile on Load Data

		// SH Change Email Shortocde
		$(document).on("click", ".sh-change-email-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var emailAddress = currentForm.find("#email-address");
			var password = currentForm.find("#password");
			if ( !emailAddress.val() ) {
				currentForm.find(".alert-danger").html("Email address is required field.").show();
				return false;
			}
			if ( !validateEmail(emailAddress.val()) ) {
				currentForm.find(".alert-danger").html("Email address is not valid. Please try again.").show();
				return false;
			}
			var inputData = {
				"new_email": emailAddress.val(),
				"confirm_domain": extranet_bluelynx_pages_array.domain_url
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/profile/email",
				"method": "POST",
				"timeout": 0,
				"headers": {
					"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					"Content-Type": "application/json",
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				// location.href = extranet_bluelynx_pages_array.login_page + "?change-email=true";
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Change Email Shortocde

		// SH Change Password Shortocde
		$(document).on("click", ".sh-change-password-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var password = currentForm.find("#password");
			var confirmPassword = currentForm.find("#confirm-password");
			if ( !password.val() ) {
				currentForm.find(".alert-danger").html("Password is required field.").show();
				return false;
			}
			if ( !validatePassword(password.val()) ) {
				var html = "Passwod must be contain following: <br>";
				html += "Min 10 characters <br>";
				html += "At least 1 one lowercase letter <br>";
				html += "At least one uppercase letter <br>";
				html += "At least one digit <br>";
				html += "At least one special character";
				currentForm.find(".alert-danger").html(html).show();
				return false;
			}
			if ( !confirmPassword.val() ) {
				currentForm.find(".alert-danger").html("Confirm Password is required field.").show();
				return false;
			}
			if ( !validatePassword(confirmPassword.val()) ) {
				var html = "Confirm Passwod must be contain following: <br>";
				html += "Min 10 characters <br>";
				html += "At least 1 one lowercase letter <br>";
				html += "At least one uppercase letter <br>";
				html += "At least one digit <br>";
				html += "At least one special character";
				currentForm.find(".alert-danger").html(html).show();
				return false;
			}
			if ( password.val() != confirmPassword.val() ) {
				currentForm.find(".alert-danger").html("Password and Confirm Password should be same.").show();
				return false;
			}
			var inputData = {
				"password": password.val(),
				"password_confirmation": confirmPassword.val(),
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/profile/password",
				"method": "POST",
				"timeout": 0,
				"headers": {
					"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					"Content-Type": "application/json",
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				location.href = extranet_bluelynx_pages_array.login_page + "?change-password=true";
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH Change Password Shortocde

		// SH GDPR Shortocde
		$(document).on("click", ".sh-gdpr-form .submit-btn", function() {
			var currentForm = $(this).closest("form");
			currentForm.find(".alert").html("").hide();
			currentForm.find(".alert-danger").html("").hide();
			var gdpr = currentForm.find("#gdpr");
			if ( !gdpr.val() ) {
				currentForm.find(".alert-danger").html("Please select an option then continue.").show();
				return false;
			}
			var inputData = {
				"kind": gdpr.val(),
			};
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/profile/gdpr",
				"method": "POST",
				"timeout": 0,
				"headers": {
					"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					"Content-Type": "application/json",
				},
				"data": JSON.stringify(inputData),
			};
			$.ajax(settings).done(function (response) {
				location.href = extranet_bluelynx_pages_array.gdpr_page + "?success=true";
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					currentForm.find(".alert-danger").html(html).show();
					return false;
				}
			});
		});
		// SH GDPR Shortocde

		// SH APPLY SHORTCODE
		if ($(document).find(".sh-apply-shortcode").length > 0) {
			if (localStorage.getItem("tokenBearer")) {
				var applyBtnHref = $(document).find(".apply-bttn:first").attr("href");
				var jobID = $.urlParam(applyBtnHref, 'RequestID');
				var jobRequestID = null;
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/job-requests/" + jobID,
					"method": "GET",
					"timeout": 0,
					"headers": {
						"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
						"Content-Type": "application/json",
					},
				};
				$.ajax(settings).done(function (response) {
					var job_request_application = response.job_request_application;
					jobRequestID = job_request_application.id;
					if (jobRequestID) {
						$(document).find(".sh-apply-shortcode .btn-apply").hide();
						$(document).find(".sh-apply-shortcode .btn-remove").data("jobRequestID", jobRequestID);
					} else {
						$(document).find(".sh-apply-shortcode .btn-remove").hide();
					}
					// location.href = extranet_bluelynx_pages_array.gdpr_page + "?success=true";
				})
				.error(function (xhr, ajaxOptions, thrownError) {
					if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
						var html = "There are following errors from the server: <br>";
						html += xhr.responseJSON.error;
						$(document).find(".sh-apply-shortcode .alert-danger").html(html).show();
						return false;
					}
				});
			} else {
				$(document).find(".sh-apply-shortcode .btn-remove").hide();
			}
			
			// APPLY JOB BUTTON FUNCTION
			$(document).on("click", ".sh-apply-shortcode .btn-apply", function() {
				if (localStorage.getItem("tokenBearer")) {
					var settings = {
						"url": "https://bluei2.bluelynx.com/api/job-requests/" + jobID + "/apply",
						"method": "POST",
						"timeout": 0,
						"headers": {
							"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
							"Content-Type": "application/json",
						},
					};
					$.ajax(settings).done(function (response) {
						location.reload();
						// location.href = extranet_bluelynx_pages_array.gdpr_page + "?success=true";
					})
					.error(function (xhr, ajaxOptions, thrownError) {
						if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
							var html = "There are following errors from the server: <br>";
							html += xhr.responseJSON.error;
							$(document).find(".sh-apply-shortcode .alert-danger").html(html).show();
							return false;
						}
					});
				} else {
					location.href = extranet_bluelynx_pages_array.login_page + "?apply-needs-login=true";
				}
			});
			// APPLY JOB BUTTON FUNCTION
			// REMOVE JOB BUTTON FUNCTION
			$(document).on("click", ".sh-apply-shortcode .btn-remove", function() {
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/job-applications/" + $(this).attr("data-jobrequestid"),
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
						"Content-Type": "application/json",
					},
				};
				$.ajax(settings).done(function (response) {
					location.reload();
					// location.href = extranet_bluelynx_pages_array.gdpr_page + "?success=true";
				})
				.error(function (xhr, ajaxOptions, thrownError) {
					if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
						var html = "There are following errors from the server: <br>";
						html += xhr.responseJSON.error;
						$(document).find(".sh-apply-shortcode .alert-danger").html(html).show();
						return false;
					}
				});
			});
			// REMOVE JOB BUTTON FUNCTION
		}
		// SH APPLY SHORTCODE

		// SH MY JOBS SHORTCODE
		if ($(document).find(".sh-my-jobs-shortcode").length > 0) {
			var settings = {
				"url": "https://bluei2.bluelynx.com/api/job-applications",
				"method": "GET",
				"timeout": 0,
				"headers": {
					"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
					"Content-Type": "application/json",
				},
			};
			$.ajax(settings).done(function (response) {
				var results = response.results;
				var counter = 1;
				results.forEach(element => {
					var html = '<tr><th scope="row">'+counter+'</th>';
					html += '<td>'+element.job_title+'</td>';
					html += '<td>'+element.application_date+'</td>';
					html += '<td>'+element.request_nr+'</td>';
					var categories = [];
					element.categories.forEach(category => {
						categories.push(category.name);
					});
					html += '<td>'+categories.join(", ")+'</td>';
					html += '<td>'+element.location+'</td>';
					html += '<td>'+element.status+'</td>';
					html += '<td><button type="button" class="btn btn-primary remove-request-btn" data-jobRequestID="'+element.id+'">Remove Request</button></td></tr>';
					counter++;
					$(document).find(".sh-my-jobs-shortcode .my-jobs-table tbody").append(html);
				});
				// $(document).find(".sh-my-jobs-shortcode .my-jobs-table").DataTable();
			})
			.error(function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
					var html = "There are following errors from the server: <br>";
					html += xhr.responseJSON.error;
					$(document).find(".sh-my-jobs-shortcode .alert-danger").html(html).show();
					return false;
				}
			});
			// REMOVE JOB REQUEST BUTTON FUNCTION
			$(document).on("click", ".sh-my-jobs-shortcode .remove-request-btn", function() {
				var settings = {
					"url": "https://bluei2.bluelynx.com/api/job-applications/" + $(this).attr("data-jobrequestid"),
					"method": "DELETE",
					"timeout": 0,
					"headers": {
						"Authorization": "Bearer " + localStorage.getItem("tokenBearer"),
						"Content-Type": "application/json",
					},
				};
				$.ajax(settings).done(function (response) {
					location.href = extranet_bluelynx_pages_array.my_jobs_page + "?remove-success=true";
				})
				.error(function (xhr, ajaxOptions, thrownError) {
					if (xhr.responseJSON.error && xhr.responseJSON.error.length > 0) {
						var html = "There are following errors from the server: <br>";
						html += xhr.responseJSON.error;
						$(document).find(".sh-apply-shortcode .alert-danger").html(html).show();
						return false;
					}
				});
			});
			// REMOVE JOB REQUEST BUTTON FUNCTION
		}
		// SH MY JOBS SHORTCODE

		// LOGOUT FUNCTIONATLITY
		$(document).on("click", ".btn-logout", function() {
			var loginURL = $(this).data("login-url");
			localStorage.clear();
			location.href = loginURL + "?logout=true";
		});
		// LOGOUT FUNCTIONATLITY

		// SH NAV SHORTCODE
		if ($(document).find(".sh-nav-shortcode").length > 0) {
			if (localStorage.getItem("tokenBearer")) {
				$(document).find(".sh-nav-shortcode .not-logged-in").hide();
			} else {
				$(document).find(".sh-nav-shortcode .logged-in").hide();
			}
		}
		// SH NAV SHORTCODE

		// Save and Continue Button
		$(document).on("click", ".save-and-continue-btn", function() {
			var tab = $(this).data("tab") + "-tab";
			$(document).find("a"+tab).click();
			$('html, body').animate({
				scrollTop: $(document).find(".nav.nav-pills").offset().top
			}, 1000);
		});
		// Save and Continue Button
	});

})( jQuery );
