<div class="sh-change-password-shortcode bootstrapiso">
    <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-change-password-form">
        <div class="alert alert-danger" role="alert"></div>
        <p class="card-text">Please enter new password to update.</p>
        <div class="row">
            <label for="password" class="col-sm-2 col-form-label">New Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" data-title="New Password" id="password">
            </div>
        </div>
        <div class="row">
            <label for="confirm-password" class="col-sm-2 col-form-label">Confirm New Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control-plaintext" data-title="Confirm New Password" id="confirm-password">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 mb-3">
                <button type="button" class="btn btn-success btn-block submit-btn">UPDATE PASSWORD</button>
            </div>
        </div>
    </form>
</div>