<?php
    $extranetBlueLynxPluginSettings = get_option('extranet_blue_lynx_plugin_settings', array());
    $login_page_id = $extranetBlueLynxPluginSettings['login_page_id'];
    $register_page_id = $extranetBlueLynxPluginSettings['register_page_id'];
    $sorry_page_id = $extranetBlueLynxPluginSettings['sorry_page_id'];
    $confirm_email_page_id = $extranetBlueLynxPluginSettings['confirm_email_page_id'];
    $questionnaire_page_id = $extranetBlueLynxPluginSettings['questionnaire_page_id'];
    $upload_cv_page_id = $extranetBlueLynxPluginSettings['upload_cv_page_id'];
    $profile_page_id = $extranetBlueLynxPluginSettings['profile_page_id'];
    $dashboard_page_id = $extranetBlueLynxPluginSettings['dashboard_page_id'];
    $change_email_page_id = $extranetBlueLynxPluginSettings['change_email_page_id'];
    $change_password_page_id = $extranetBlueLynxPluginSettings['change_password_page_id'];
    $gdpr_page_id = $extranetBlueLynxPluginSettings['gdpr_page_id'];
    $my_jobs_page_id = $extranetBlueLynxPluginSettings['my_jobs_page_id'];
?>
<div class="sh-nav-shortcode bootstrapiso text-right">
    <nav>
        <ul class="sf-menu sf-js-enabled sf-arrows not-logged-in">
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="<?php echo get_permalink((int) $login_page_id); ?>" class="sf-with-ul">Login</a>
            </li>
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="<?php echo get_permalink((int) $register_page_id); ?>" class="sf-with-ul">Register</a>
            </li>
        </ul>
        <ul class="sf-menu sf-js-enabled sf-arrows logged-in">
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="<?php echo get_permalink((int) $dashboard_page_id); ?>" class="sf-with-ul">Dashboard</a>
            </li>
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="<?php echo get_permalink((int) $my_jobs_page_id); ?>" class="sf-with-ul">My Jobs</a>
            </li>
            <li class="menu-item menu-item-type-custom menu-item-object-custom">
                <a href="#" data-login-url="<?php echo get_permalink((int) $login_page_id); ?>" class="sf-with-ul btn-logout">Logout</a>
            </li>
        </ul>
    </nav>
</div>