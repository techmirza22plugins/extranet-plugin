<div class="sh-gdpr-shortcode bootstrapiso">
    <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-gdpr-form">
        <div class="alert alert-danger" role="alert"></div>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') { ?>
            <div class="alert alert-success" role="alert" style="display: block;">Your GDPR request has been sent to the system successfully.</div>
        <?php } ?>
        <select name="gdpr" id="gdpr" class="form-control">
            <option value="">Select Removal Option</option>
            <option value="delete_data">Delete all my data.</option>
            <option value="delete_data_and_profile">Partial removal of data, keep login details but delete my data.</option>
            <option value="do_not_contact">I want to remain registered but do not want to contacted for postions until I request this.</option>
        </select>
        <div class="row mt-4">
            <div class="col-sm-12 mb-3">
                <button type="button" class="btn btn-success btn-block submit-btn">SEND REQUEST</button>
            </div>
        </div>
    </form>
</div>