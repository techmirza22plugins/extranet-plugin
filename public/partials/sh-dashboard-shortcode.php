<?php
    $extranetBlueLynxPluginSettings = get_option('extranet_blue_lynx_plugin_settings', array());
    $upload_cv_page_id = $extranetBlueLynxPluginSettings['upload_cv_page_id'];
    $profile_page_id = $extranetBlueLynxPluginSettings['profile_page_id'];
    $change_email_page_id = $extranetBlueLynxPluginSettings['change_email_page_id'];
    $change_password_page_id = $extranetBlueLynxPluginSettings['change_password_page_id'];
    $my_jobs_page_id = $extranetBlueLynxPluginSettings['my_jobs_page_id'];
    $gdpr_page_id = $extranetBlueLynxPluginSettings['gdpr_page_id'];
?>
<div class="sh-dashboard-shortcode bootstrapiso">
    <a href="<?php echo get_permalink( (int) $my_jobs_page_id ); ?>" class="btn btn-primary btn-lg btn-block">My Jobs Dashboard</a>
    <a href="<?php echo get_permalink( (int) $gdpr_page_id ); ?>" class="btn btn-primary btn-lg btn-block">Submit GDPR</a>
    <a href="<?php echo get_permalink( (int) $profile_page_id ); ?>" class="btn btn-primary btn-lg btn-block">Profile</a>
    <a href="<?php echo get_permalink( (int) $upload_cv_page_id ); ?>" class="btn btn-primary btn-lg btn-block">Upload New CV</a>
    <a href="<?php echo get_permalink( (int) $change_email_page_id ); ?>" class="btn btn-primary btn-lg btn-block">Change Email</a>
    <a href="<?php echo get_permalink( (int) $change_password_page_id ); ?>" class="btn btn-primary btn-lg btn-block">Change Password</a>
    <button type="button" data-login-url="<?php echo get_permalink( (int) $gdpr_page_id ); ?>" class="btn btn-primary btn-lg btn-block btn-logout">Logout</button>
</div>