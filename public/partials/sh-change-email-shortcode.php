<div class="sh-change-email-shortcode bootstrapiso">
    <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-change-email-form">
        <div class="alert alert-danger" role="alert"></div>
        <p class="card-text">Please enter new email address to update.</p>
        <div class="row">
            <label for="email-address" class="col-sm-2 col-form-label">E-mail Address</label>
            <div class="col-sm-10">
                <input type="email" class="form-control-plaintext" data-title="Email Address" id="email-address">
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-sm-12 mb-3">
                <button type="button" class="btn btn-success btn-block submit-btn">CHANGE EMAIL</button>
            </div>
        </div>
    </form>
</div>