<div class="sh-forgot-password-shortcode">
    <div class="card shadow-sm bg-white rounded">
        <div class="card-body">
            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-forgot-password-form">
                <div class="alert alert-success" role="alert"></div>
                <div class="alert alert-danger" role="alert"></div>
                <p class="card-text">Please enter your email address below to reset the password.</p>
                <input type="hidden" name="action" value="sh_register_submit">
                <div class="row">
                    <label for="email-address" class="col-sm-2 col-form-label">E-mail Address</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control-plaintext" data-title="Email Address" id="email-address">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 mb-3">
                        <button type="button" class="btn btn-success btn-block submit-btn">SUBMIT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>