<div class="sh-upload-cv-shortcode bootstrapiso">
    <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'true') : ?>
    <div class="alert alert-success" role="alert">
        Thanks for registering on Blue Lynx. Please upload your CV to continue further.
    </div>
    <?php endif; ?>
    <div class="alert alert-danger" role="alert"></div>
    <div class="drop-zone">
        <span class="drop-zone__prompt">Drop file here or click to upload</span>
        <input type="file" name="myFile" id="drop-zone-input" class="drop-zone__input">
    </div>
    <button type="button" class="btn btn-danger btn-remove-file mt-3 btn-block"><strong>Remove File</strong></button>
    <button type="button" class="btn btn-warning btn-submit btn-block"><strong>Upload and Continue</strong></button>
</div>