<div class="sh-questionnaire-shortcode bootstrapiso">
    <div class="quiz quiz-1 mt-3" data-toggle="buttons">
        <h3 class="mb-3">Step 1. Where do you live?</h3>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="location" value="1">Europe</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="location" value="2">Outside Europe</label>
    </div>
    <div class="quiz quiz-2 hidden mt-3" data-toggle="buttons">
        <h3 class="mb-3">Step 2. Are you an EU passport holder?</h3>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="passport" value="1">Yes</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="passport" value="2">No</label>
    </div>
    <div class="quiz quiz-3 hidden mt-3" data-toggle="buttons">
        <h3 class="mb-3">Step 3. Are you eligible to work in the EU?</h3>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="eligible" value="1">Yes</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="eligible" value="2">No Europe</label>
    </div>
    <div class="quiz quiz-4 hidden final-quiz mt-5" data-toggle="buttons">
        <h3 class="mb-3">Step 4. Please select the appropriate option below:</h3>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="1">Spouse Visa</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="2">Knowledge Worker</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="3">Partner of Knowledge Worker</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="4">Are you/or your knowledge migrant partner in the process of applying for a permit</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="5">Holiday (Working) Visa</label>
        <label class="element-animation btn btn-lg btn-primary btn-block"><span class="btn-label"><i class="glyphicon glyphicon-chevron-right"></i></span> <input type="radio" name="reason" value="6">None of Above</label>
    </div>
</div>