<div class="sh-reset-password-shortcode">
    <div class="card shadow-sm bg-white rounded">
        <div class="card-body">
            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-reset-password-form">
                <div class="alert alert-danger" role="alert"></div>
                <div class="alert alert-success" role="alert"></div>
                <p class="card-text">Please enter new password below to update your account.</p>
                <input type="hidden" name="action" value="sh_reset_password_submit">
                <div class="row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control-plaintext" data-title="Password" id="password">
                    </div>
                </div>
                <div class="row">
                    <label for="confirm-password" class="col-sm-2 col-form-label">Confirm Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control-plaintext" data-title="Confirm Password" id="confirm-password">
                    </div>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 mb-3">
                        <button type="button" class="btn btn-success btn-block submit-btn">RESET PASSWORD</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>