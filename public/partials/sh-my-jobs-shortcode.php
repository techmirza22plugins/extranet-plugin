<div class="sh-my-jobs-shortcode bootstrapiso">
    <?php if (isset($_GET['remove-success']) && !empty($_GET['remove-success']) && $_GET['remove-success'] == 'true') { ?>
        <div class="alert alert-success" role="alert" style="display: block;">Your job request has been removed successfully.</div>
    <?php } ?>
    <div class="alert alert-danger"></div>
    <table class="table my-jobs-table">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Date</th>
                <th scope="col">Ref</th>
                <th scope="col">Category</th>
                <th scope="col">Region</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>