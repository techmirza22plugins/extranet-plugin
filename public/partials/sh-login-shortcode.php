<div class="sh-login-shortcode">
    <div class="card shadow-sm bg-white rounded">
        <div class="card-body">
            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-login-form">
                <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">Thank you for registration. Please confirm the email you've received in your inbox.</div>
                <?php } ?>
                <?php if (isset($_GET['reset-password']) && !empty($_GET['reset-password']) && $_GET['reset-password'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">You've successfully changed the password. Please login to your account with updated password.</div>
                <?php } ?>
                <?php if (isset($_GET['confirm']) && !empty($_GET['confirm']) && $_GET['confirm'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">Your email address is confirmed. Please login to your account with updated password.</div>
                <?php } ?>
                <?php if (isset($_GET['change-email']) && !empty($_GET['change-email']) && $_GET['change-email'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">Your email address has been changed. Please confirm the email you've received in your inbox.</div>
                <?php } ?>
                <?php if (isset($_GET['change-password']) && !empty($_GET['change-password']) && $_GET['change-password'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">Your password has been changed. Please login to your account with updated password.</div>
                <?php } ?>
                <?php if (isset($_GET['logout']) && !empty($_GET['logout']) && $_GET['logout'] == 'true') { ?>
                    <div class="alert alert-success" role="alert" style="display: block;">You've successfully logged out from the system.</div>
                <?php } ?>
                <?php if (isset($_GET['apply-needs-login']) && !empty($_GET['apply-needs-login']) && $_GET['apply-needs-login'] == 'true') { ?>
                    <div class="alert alert-danger" role="alert" style="display: block;">Please login to your account in order to apply for a job.</div>
                <?php } ?>
                <div class="alert alert-danger" role="alert"></div>
                <p class="card-text">Please enter your email address and password below to login.</p>
                <input type="hidden" name="action" value="sh_login_submit">
                <div class="row">
                    <label for="email-address" class="col-sm-2 col-form-label">E-mail Address</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control-plaintext" data-title="Email Address" id="email-address">
                    </div>
                </div>
                <div class="row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control-plaintext" data-title="Password" id="password">
                    </div>
                </div>
                <strong class="mb-2">Privacy Policy / GDPR <a href="#">(Read More)</a></strong>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="privacy-policy" checked>
                    <label class="form-check-label" for="privacy-policy">
                        By checking this box I consent to Blue Lynx privacy policy.
                    </label>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 mb-3">
                        <button type="button" class="btn btn-success btn-block submit-btn">LOGIN</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-warning btn-block reset-password-btn">RESET PASSWORD</button>
                    </div>
                    <div class="col-sm-6">
                        <button type="button" class="btn btn-warning btn-block forgot-password-btn">FORGOT PASSWORD</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>