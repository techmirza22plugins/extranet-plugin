<div class="sh-register-shortcode bootstrapiso">
    <div class="card shadow-sm bg-white rounded">
        <div class="card-body">
            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" class="sh-register-form">
                <div class="alert alert-danger" role="alert"></div>
                <p class="card-text">Please enter your email address and password below to create your account.</p>
                <input type="hidden" name="action" value="sh_register_submit">
                <div class="row">
                    <label for="last-name" class="col-sm-2 col-form-label">Last Name</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control-plaintext" data-title="Last Name" id="last-name">
                    </div>
                </div>
                <div class="row">
                    <label for="email-address" class="col-sm-2 col-form-label">E-mail Address</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control-plaintext" data-title="Email Address" id="email-address">
                    </div>
                </div>
                <div class="row">
                    <label for="password" class="col-sm-2 col-form-label">Password</label>
                    <div class="col-sm-10">
                        <input type="password" class="form-control-plaintext" data-title="Password" id="password">
                    </div>
                </div>
                <strong class="mb-2">Privacy Policy / GDPR <a href="#">(Read More)</a></strong>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="privacy-policy" checked>
                    <label class="form-check-label" for="privacy-policy">
                        By checking this box I consent to Blue Lynx privacy policy.
                    </label>
                </div>
                <div class="row mt-4">
                    <div class="col-sm-12 mb-3">
                        <button type="button" class="btn btn-success btn-block submit-btn">CREATE ACCOUNT</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>