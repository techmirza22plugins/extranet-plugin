<div class="sh-profile-shortcode bootstrapiso">
    <?php if (isset($_GET['updated']) && !empty($_GET['updated']) && $_GET['updated'] == 'true') { ?>
        <div class="alert alert-success">Congractulations! You're profile has been updated successfully.</div>
    <?php } ?>
    <p><strong>Fill the following information in order to complete the profile.</strong></p>
    <ul class="nav nav-pills mb-3 ml-0" id="pills-tab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="pills-personalia-tab" data-toggle="pill" href="#pills-personalia" role="tab" aria-controls="pills-personalia" aria-selected="true">Personalia</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-language-and-skills-tab" data-toggle="pill" href="#pills-language-and-skills" role="tab" aria-controls="pills-language-and-skills" aria-selected="false">Language and Skills</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-education-tab" data-toggle="pill" href="#pills-education" role="tab" aria-controls="pills-education" aria-selected="false">Education</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="pills-job-experience-tab" data-toggle="pill" href="#pills-job-experience" role="tab" aria-controls="pills-job-experience" aria-selected="false">Job Experience</a>
        </li>
    </ul>
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-personalia" role="tabpanel" aria-labelledby="pills-personalia-tab">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control first-name" id="first_name" data-field="First Name">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="middle_name">Middle Name</label>
                        <input type="text" class="form-control middle-name" id="middle_name" data-field="Middle Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name_preposition">Last Name Preposition</label>
                        <input type="text" class="form-control last-name-preposition" id="last_name_preposition" data-field="Last Name Preposition">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control last-name" id="last_name" data-field="Last Name">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="initials">Initials</label>
                        <input type="text" class="form-control initials" id="initials" data-field="Initials">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="gender">Gender</label>
                        <select name="gender" id="gender" class="form-control gender"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth</label>
                        <input type="date" class="form-control date-of-birth" id="date_of_birth" data-field="Date of Birth">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="nationalities">Nationalities</label>
                        <select name="nationalities" id="nationalities" class="form-control nationalities select2" data-field="Nationalities" multiple></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="country">Country</label>
                        <select name="country" id="country" class="form-control country" data-field="Country"></select>
                    </div>
                </div>
                <div class="col-sm-6 country-region-col">
                    <div class="form-group">
                        <label for="country_region">County / Province</label>
                        <input type="text" class="form-control country-region" id="country_region" data-field="County / Province">
                    </div>
                </div>
                <div class="col-sm-6 country-region-uk-col">
                    <div class="form-group">
                        <label for="country_region_uk">County / Province</label>
                        <select name="country" id="country_region_uk" class="form-control country-region-uk" data-field="County / Province"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control address" id="address" data-field="Address">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="housenumber">House Number</label>
                        <input type="text" class="form-control housenumber" id="housenumber" data-field="House Number">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="postcode">Post Code</label>
                        <input type="text" class="form-control postcode" id="postcode" data-field="Post Code">
                    </div>
                </div>
                <div class="col-sm-3 town-col">
                    <div class="form-group">
                        <label for="town">Town</label>
                        <input type="text" class="form-control town" id="town" data-field="Town">
                    </div>
                </div>
                <div class="col-sm-3 nl-town-col">
                    <div class="form-group">
                        <label for="nl_town">Town</label>
                        <select name="nl_town" id="nl_town" class="form-control nl-town" data-field="Town"></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="address2">Address / Street 2</label>
                        <input type="text" class="form-control address2" id="address2" data-field="Address / Street 2">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="phone_home">Tel. Home</label>
                        <input type="text" class="form-control phone-home" id="phone_home" data-field="Tel. Home">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="phone_mobile">Tel. Mobile</label>
                        <input type="text" class="form-control phone-mobile" id="phone_mobile" data-field="Tel. Mobile">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="linked_in">LinkedIN</label>
                        <input type="text" class="form-control linked-in" id="linked_in" data-field="LinkedIN">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="email">E-Mail</label>
                        <input type="email" class="form-control email" id="email" data-field="E-Mail">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="personal_website">Personal Website</label>
                        <input type="text" class="form-control personal-website" id="personal_website" data-field="Personal Website">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="drivers_license">Drivers License</label>
                        <input type="text" class="form-control drivers-license" id="drivers_license" data-field="Drivers License">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="preferred_countries">Preferred Countries</label>
                        <select name="preferred_countries" id="preferred_countries" class="form-control preferred-countries select2" data-field="Preferred Country" multiple></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="salary_range_min">Salary Min</label>
                        <input type="number" class="form-control salary-range-min" id="salary_range_min" data-field="Salary Min">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="salary_range_max">Salary Max</label>
                        <input type="number" class="form-control salary-range-max" id="salary_range_max" data-field="Salary Max">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="salary_currency">Salary Currency</label>
                        <select name="salary_currency" class="form-control salary-currency" id="salary_currency" data-field="Salary Currency"></select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="">Salary Range</label>
                        <div class="salary-types-container"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3 pt-4">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input salary-includes-vacation" id="salary_includes_vacation">
                            <label class="custom-control-label" for="salary_includes_vacation">Includes Vacation</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 pt-4">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input salary-is-negotiable" id="salary_is_negotiable">
                            <label class="custom-control-label" for="salary_is_negotiable">Is Negotiable?</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 pt-4">
                    <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input freelancer-interest" id="freelancer_interest">
                            <label class="custom-control-label" for="freelancer_interest">Freelancer Interest?</label>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="freelancer_day_rate">Freelancer Day Rate</label>
                        <input type="text" class="form-control freelancer-day-rate" id="freelancer_day_rate" data-field="Freelancer Day Rate">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="type_of_employment">Employment Type</label>
                        <select name="type_of_employment" class="form-control type-of-employment" id="type_of_employment" data-field="Employment Type"></select>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label for="full_parttime">Job Type</label>
                        <select name="full_parttime" class="form-control full-parttime" id="full_parttime" data-field="Job Type"></select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="travelling_time_id">Max. Commute Time</label>
                        <select name="travelling_time_id" class="form-control travelling-time-id" id="travelling_time_id" data-field="Job Type"></select>
                    </div>
                </div>
            </div>
            <button class="btn btn-primary btn-block save-and-continue-btn" type="button" data-tab="#pills-language-and-skills">Save and Continue</a>
        </div>
        <div class="tab-pane fade" id="pills-language-and-skills" role="tabpanel" aria-labelledby="pills-language-and-skills-tab">
            <h4>Languages</h4>
            <button type="button" class="btn btn-primary btn-block add-new-lang-btn">Add New Language</button>
            <div class="languages-container"></div>
            <hr>
            <h4>Skills</h4>
            <div class="skills-container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="software">Software Skills</label>
                            <select name="software" id="software" class="form-control software-skills select2 tags" data-field="Software Skills" multiple></select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="soft_skills">Soft Skills</label>
                            <select name="soft_skills" id="soft_skills" class="form-control soft-skills select2 tags" data-field="Soft Skills" multiple></select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="professional_skills">Professional Skills</label>
                            <select name="professional_skills" id="professional_skills" class="form-control professional-skills select2 tags" data-field="Professional Skills" multiple></select>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="hobbies">Hobbies</label>
                            <select name="hobbies" id="hobbies" class="form-control hobbies select2 tags" data-field="Hobbies" multiple></select>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="ambitions">Ambitions</label>
                            <textarea name="ambitions" id="ambitions" class="form-control ambitions" cols="30" rows="30"></textarea>
                        </div>
                    </div>
                </div>
                <button class="btn btn-primary btn-block save-and-continue-btn" type="button" data-tab="#pills-education">Save and Continue</a>
            </div>
        </div>
        <div class="tab-pane fade" id="pills-education" role="tabpanel" aria-labelledby="pills-education-tab">
            <div class="form-group">
                <label for="highest_education_id">Highest Education</label>
                <select name="highest_education_id" id="highest_education_id" class="form-control highest-education-id" data-field="Highest Education"></select>
            </div>
            <button type="button" class="btn btn-primary btn-block add-new-education-btn">Add New Education</button>
            <div class="educations-container"></div>
            <button class="btn btn-primary btn-block save-and-continue-btn mt-3" type="button" data-tab="#pills-job-experience">Save and Continue</a>
        </div>
        <div class="tab-pane fade" id="pills-job-experience" role="tabpanel" aria-labelledby="pills-job-experience-tab">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="companies">Companies</label>
                        <select class="form-control companies select2 tags" id="companies" data-field="Companies" multiple><select>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="industries">Industries</label>
                        <select class="form-control industries select2 tags" id="industries" data-field="Industries" multiple><select>
                    </div>
                </div>
            </div>
            <button type="button" class="btn btn-primary btn-block add-new-job-experience-btn">Add New Job Experience</button>
            <div class="job-experiences-container mb-3"></div>
            <button type="button" class="btn btn-primary btn-block update-profile-btn">Update Profile</button>
        </div>
    </div>
</div>