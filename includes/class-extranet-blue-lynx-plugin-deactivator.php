<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Extranet_Blue_Lynx_Plugin_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
