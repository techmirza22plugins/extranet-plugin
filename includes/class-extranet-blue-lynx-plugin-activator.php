<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Extranet_Blue_Lynx_Plugin_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
