<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $tab = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : "instructions";
?>

<div class="wrap extranet-blue-lynx-plugin-settings-div">
    <h2><?php _e( 'Extranet Blue Lynx Plugin Settings', 'extranet-blue-lynx-plugin' ); ?></h2>
    <?php settings_errors(); ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=extranet-blue-lynx-plugin-settings&tab=instructions'); ?>" class="nav-tab <?php echo ($tab=='instructions') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Instructions', 'extranet-blue-lynx-plugin' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=extranet-blue-lynx-plugin-settings&tab=page-settings'); ?>" class="nav-tab <?php echo ($tab=='page-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Page Settings', 'extranet-blue-lynx-plugin' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=extranet-blue-lynx-plugin-settings&tab=variable-settings'); ?>" class="nav-tab <?php echo ($tab=='variable-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Variable Settings', 'extranet-blue-lynx-plugin' ); ?>
        </a>
    </h2>
    <?php if ($tab == 'instructions') { ?>
        <ul>
            <li>Following is the list of shortcode for <strong>Extranet Blue Lynx System</strong> which can be easily integrated into the pages.</li>
            <li>1. <code>[sh-login]</code> can be placed anywhere in the page to load <strong>Login Page</strong>.</li>
            <li>2. <code>[sh-register]</code> can be placed anywhere in the page to load <strong>Register Page</strong>.</li>
            <li>3. <code>[sh-forgot-password]</code> can be placed anywhere in the page to load <strong>Forget Password Page</strong>.</li>
            <li>4. <code>[sh-reset]</code> can be placed anywhere in the page to load <strong>Reset Password Page</strong>.</li>
            <li>5. <code>[sh-questionnaire]</code> can be placed anywhere in the page to load <strong>Questionnaire Page</strong>.</li>
            <li>6. <code>[sh-upload-cv]</code> can be placed anywhere in the page to load <strong>Upload CV Page</strong>.</li>
            <li>7. <code>[sh-profile]</code> can be placed anywhere in the page to load <strong>Profile Page</strong>.</li>
            <li>8. <code>[sh-dashboard]</code> can be placed anywhere in the page to load <strong>Dashboard Page</strong>.</li>
            <li>9. <code>[sh-change-email]</code> can be placed anywhere in the page to load <strong>Change Email Page</strong>.</li>
            <li>10. <code>[sh-change-password]</code> can be placed anywhere in the page to load <strong>Change Password Page</strong>.</li>
            <li>11. <code>[sh-gdpr]</code> can be placed anywhere in the page to load <strong>GDPR Page</strong>.</li>
            <li>12. <code>[sh-apply]</code> can be placed anywhere in the page to load <strong>Apply/Remove Button for Job Page</strong>.</li>
            <li>13. <code>[sh-my-jobs]</code> can be placed anywhere in the page to load <strong>My Jobs Page</strong>.</li>
        </ul>
    <?php } ?>
    <?php if ($tab == 'page-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'extranet-blue-lynx-plugin-settings' );
            do_settings_sections( 'extranet-blue-lynx-plugin-settings' );
            $extranetBlueLynxPluginSettings = get_option('extranet_blue_lynx_plugin_settings', array());
            $login_page_id = $extranetBlueLynxPluginSettings['login_page_id'];
            $register_page_id = $extranetBlueLynxPluginSettings['register_page_id'];
            $forgot_password_page_id = $extranetBlueLynxPluginSettings['forgot_password_page_id'];
            $reset_password_page_id = $extranetBlueLynxPluginSettings['reset_password_page_id'];
            $sorry_page_id = $extranetBlueLynxPluginSettings['sorry_page_id'];
            $confirm_email_page_id = $extranetBlueLynxPluginSettings['confirm_email_page_id'];
            $questionnaire_page_id = $extranetBlueLynxPluginSettings['questionnaire_page_id'];
            $upload_cv_page_id = $extranetBlueLynxPluginSettings['upload_cv_page_id'];
            $profile_page_id = $extranetBlueLynxPluginSettings['profile_page_id'];
            $dashboard_page_id = $extranetBlueLynxPluginSettings['dashboard_page_id'];
            $change_email_page_id = $extranetBlueLynxPluginSettings['change_email_page_id'];
            $change_password_page_id = $extranetBlueLynxPluginSettings['change_password_page_id'];
            $gdpr_page_id = $extranetBlueLynxPluginSettings['gdpr_page_id'];
            $my_jobs_page_id = $extranetBlueLynxPluginSettings['my_jobs_page_id']; ?>
            <table class="widefat form-table extranet-blue-lynx-plugin-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="login_page_id"><?php _e( 'Login Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[login_page_id]" id="login_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($login_page_id) && !empty($login_page_id) && $login_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="register_page_id"><?php _e( 'Register Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[register_page_id]" id="register_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($register_page_id) && !empty($register_page_id) && $register_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="forgot_password_page_id"><?php _e( 'Forgot Password Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[forgot_password_page_id]" id="forgot_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($forgot_password_page_id) && !empty($forgot_password_page_id) && $forgot_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="reset_password_page_id"><?php _e( 'Reset Password Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[reset_password_page_id]" id="reset_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($reset_password_page_id) && !empty($reset_password_page_id) && $reset_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="sorry_page_id"><?php _e( 'Sorry Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[sorry_page_id]" id="sorry_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($sorry_page_id) && !empty($sorry_page_id) && $sorry_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="confirm_email_page_id"><?php _e( 'Confirm Email Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[confirm_email_page_id]" id="confirm_email_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($confirm_email_page_id) && !empty($confirm_email_page_id) && $confirm_email_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="questionnaire_page_id"><?php _e( 'Questionnaire Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[questionnaire_page_id]" id="questionnaire_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($questionnaire_page_id) && !empty($questionnaire_page_id) && $questionnaire_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="upload_cv_page_id"><?php _e( 'Upload CV Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[upload_cv_page_id]" id="upload_cv_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($upload_cv_page_id) && !empty($upload_cv_page_id) && $upload_cv_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="profile_page_id"><?php _e( 'Profile Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[profile_page_id]" id="profile_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($profile_page_id) && !empty($profile_page_id) && $profile_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="dashboard_page_id"><?php _e( 'Dashboard Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[dashboard_page_id]" id="dashboard_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($dashboard_page_id) && !empty($dashboard_page_id) && $dashboard_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="change_email_page_id"><?php _e( 'Change Email Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[change_email_page_id]" id="change_email_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($change_email_page_id) && !empty($change_email_page_id) && $change_email_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="change_password_page_id"><?php _e( 'Change Password Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[change_password_page_id]" id="change_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($change_password_page_id) && !empty($change_password_page_id) && $change_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="gdpr_page_id"><?php _e( 'GDPR Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[gdpr_page_id]" id="gdpr_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($gdpr_page_id) && !empty($gdpr_page_id) && $gdpr_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="my_jobs_page_id"><?php _e( 'My Jobs Page', 'extranet-blue-lynx-plugin' ); ?></label>
                        </td>
                        <td>
                            <select name="extranet_blue_lynx_plugin_settings[my_jobs_page_id]" id="my_jobs_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'extranet-blue-lynx-plugin' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($my_jobs_page_id) && !empty($my_jobs_page_id) && $my_jobs_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
    <?php if ($tab == 'variable-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'extranet-blue-lynx-variable-settings' );
            do_settings_sections( 'extranet-blue-lynx-plugin-settings' );
            $extranetBlueLynxVariableSettings = get_option('extranet_blue_lynx_variable_settings', array()); ?>
            <table class="widefat form-table extranet-blue-lynx-plugin-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="domain_url"><?php _e( 'Domain URL', 'custom-courier-driver-jps' ); ?></label>
                        </td>
                        <td>
                            <input type="text" id="domain_url" class="wd100" name="extranet_blue_lynx_variable_settings[domain_url]" value="<?php echo (isset($extranetBlueLynxVariableSettings['domain_url']) && !empty($extranetBlueLynxVariableSettings['domain_url'])) ? $extranetBlueLynxVariableSettings['domain_url'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="register_endpoint"><?php _e( 'Register Endpoint', 'custom-courier-driver-jps' ); ?></label>
                        </td>
                        <td>
                            <input type="text" id="register_endpoint" class="wd100" name="extranet_blue_lynx_variable_settings[register_endpoint]" value="<?php echo (isset($extranetBlueLynxVariableSettings['register_endpoint']) && !empty($extranetBlueLynxVariableSettings['register_endpoint'])) ? $extranetBlueLynxVariableSettings['register_endpoint'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="login_endpoint"><?php _e( 'Login Endpoint', 'custom-courier-driver-jps' ); ?></label>
                        </td>
                        <td>
                            <input type="text" id="login_endpoint" class="wd100" name="extranet_blue_lynx_variable_settings[login_endpoint]" value="<?php echo (isset($extranetBlueLynxVariableSettings['login_endpoint']) && !empty($extranetBlueLynxVariableSettings['login_endpoint'])) ? $extranetBlueLynxVariableSettings['login_endpoint'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="forgot_password_endpoint"><?php _e( 'Forgot Password Endpoint', 'custom-courier-driver-jps' ); ?></label>
                        </td>
                        <td>
                            <input type="text" id="forgot_password_endpoint" class="wd100" name="extranet_blue_lynx_variable_settings[forgot_password_endpoint]" value="<?php echo (isset($extranetBlueLynxVariableSettings['forgot_password_endpoint']) && !empty($extranetBlueLynxVariableSettings['forgot_password_endpoint'])) ? $extranetBlueLynxVariableSettings['forgot_password_endpoint'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="reset_password_endpoint"><?php _e( 'Reset Password Endpoint', 'custom-courier-driver-jps' ); ?></label>
                        </td>
                        <td>
                            <input type="text" id="reset_password_endpoint" class="wd100" name="extranet_blue_lynx_variable_settings[reset_password_endpoint]" value="<?php echo (isset($extranetBlueLynxVariableSettings['reset_password_endpoint']) && !empty($extranetBlueLynxVariableSettings['reset_password_endpoint'])) ? $extranetBlueLynxVariableSettings['reset_password_endpoint'] : ''; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
</div>
