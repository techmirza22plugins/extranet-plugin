<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Extranet_Blue_Lynx_Plugin
 * @subpackage Extranet_Blue_Lynx_Plugin/admin
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Extranet_Blue_Lynx_Plugin_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Extranet_Blue_Lynx_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Extranet_Blue_Lynx_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-select2-css', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/extranet-blue-lynx-plugin-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Extranet_Blue_Lynx_Plugin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Extranet_Blue_Lynx_Plugin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-select2-js', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/extranet-blue-lynx-plugin-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function extranet_blue_lynx_plugin_settings() {
		register_setting( 'extranet-blue-lynx-plugin-settings', 'extranet_blue_lynx_plugin_settings' );
		register_setting( 'extranet-blue-lynx-variable-settings', 'extranet_blue_lynx_variable_settings' );
	}

	public function extranet_blue_lynx_plugin_menu() {
		add_menu_page(
			'Extranet Blue Lynx Plugin Settings',
			'Extranet Blue Lynx Plugin Settings',
			'manage_options',
			'extranet-blue-lynx-plugin-settings',
			array($this, 'plugin_settings_callback')
		);
	}

	public function plugin_settings_callback() {
		require('partials/extranet-blue-lynx-plugin-admin-display.php');
	}

}
